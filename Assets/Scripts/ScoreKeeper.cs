﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int score = 0;
	private Text myText; 

	void Start(){
		myText = GetComponent<Text> ();
		ResetScore();
	}

	public void AddScore (int points){
		//Debug.Log ("Scored Points");
		score += points;
		myText.text = score.ToString ();
	}

	public static void ResetScore (){
		score = 0;
	}
}
