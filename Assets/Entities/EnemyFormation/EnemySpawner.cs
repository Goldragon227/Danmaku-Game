﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;

	public float width;
	public float height;

	public float speed = 5f;
	public float spawnDelay = 0.5f;
	
	private bool isMovingRight = true;

	//public float padding = 1f;
	
	private float xMin;
	private float xMax;

	// Use this for initialization
	void Start () {
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftBoundry= Camera.main.ViewportToWorldPoint (new Vector3 (0,0,distance));
		Vector3 rightBoundry = Camera.main.ViewportToWorldPoint (new Vector3 (1,0,distance));
		xMin = leftBoundry.x;
		xMax = rightBoundry.x;

		SpawnUntilFull ();
	}

	// Update is called once per frame
	void Update () {

		Movement ();
		ChangeDirection ();


		if (AllMembersDead()){
			Debug.Log("Empty Formation");
			SpawnUntilFull();
		}
	}

	public void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position, new Vector3(width,height));
	}

	//Movement

	void Movement(){
		if (isMovingRight) {
			transform.position += Vector3.right * speed * Time.deltaTime;
		} else {
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
	}

	void ChangeDirection(){
		float rightEdgeOfFormation = transform.position.x + (0.5f * width);
		float leftEdgeOfFormation = transform.position.x - (0.5f * width);
		
		if (leftEdgeOfFormation < xMin) {
			isMovingRight = true;
		} else if (rightEdgeOfFormation > xMax){
			isMovingRight = false;
		}
		float newX = Mathf.Clamp (transform.position.x, xMin, xMax);
		transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
	}
	

	bool AllMembersDead(){
		foreach (Transform childPositionGameObject in transform) {
			if (childPositionGameObject.childCount > 0){
				return false;
			}
		}
		return true;
	}

	Transform NextFreePosition(){
		foreach (Transform childPositionGameObject in transform) {
			if (childPositionGameObject.childCount == 0){
				return childPositionGameObject;
			}
		}
		return null;
	}

	void RespawnEnemies(){
		foreach(Transform child in transform){
			GameObject enemy = Instantiate (enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = child;
		}
	}

	void SpawnUntilFull(){
		Transform freePosition = NextFreePosition ();
		if (freePosition){
			GameObject enemy = Instantiate (enemyPrefab, freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
		}

		if(NextFreePosition()){
			Invoke ("SpawnUntilFull", spawnDelay);
		}
	}
}
