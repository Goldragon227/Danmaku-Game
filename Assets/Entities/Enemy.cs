﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float health = 150f;
	public float laserSpeed = 5f;
	public float fireratePerSecond = 2f;
	public int scoreValue = 150;

	public AudioClip laserSound;
	public AudioClip destroySound;


	public GameObject laser;
	private ScoreKeeper scoreKeeper;

	void Start(){
		scoreKeeper = GameObject.Find ("Score").GetComponent<ScoreKeeper> ();
	}


	void Update(){
		float probability = Time.deltaTime * fireratePerSecond;
		if (Random.value < probability){
			FireLaser ();
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		Projectile laser = collider.gameObject.GetComponent<Projectile>();
		if (laser) {
			health -= laser.GetDamage();
			laser.Hit();
			if (health <= 0){
				Death();
			}
		}
	}

	void Death(){
		AudioSource.PlayClipAtPoint (destroySound, transform.position);
		Destroy(gameObject);
		scoreKeeper.AddScore(scoreValue);
	}

	void FireLaser(){
		Vector3 startPosition = transform.position + new Vector3 (0,-1,0);
		GameObject beam = Instantiate(laser, startPosition, Quaternion.identity) as GameObject;
		beam.rigidbody2D.velocity = new Vector3(0,-laserSpeed,0);
		AudioSource.PlayClipAtPoint (laserSound, transform.position);
	}
}
