﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float shipSpeed = 20f;
	public float padding = 1f;
	public float laserSpeed;
	public float fireRateLaser;
	public float health = 250f;

	public GameObject laser;

	public AudioClip laserSound;
	public AudioClip destroySound;

	float xMin;
	float xMax;

	void Start(){
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint (new Vector3 (0,0,distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint (new Vector3 (1,0,distance));
		xMin = leftmost.x + padding;
		xMax = rightmost.x - padding;
	}

	void Update () {

		MovementUpdate ();

		if (Input.GetKeyDown(KeyCode.Space)){
			InvokeRepeating("FireLaser", 0.000001f, fireRateLaser);
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			CancelInvoke("FireLaser");
		}

	}

	void MovementUpdate(){
		if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift)){
			transform.position += Vector3.left * (shipSpeed/4f) * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift)){
			transform.position += Vector3.right * (shipSpeed/4f) * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.A)){
			transform.position += Vector3.left * shipSpeed * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.D)) {
			transform.position += Vector3.right * shipSpeed * Time.deltaTime;
		}  
		//Restricts the player to to max and min
		float newX = Mathf.Clamp (transform.position.x, xMin, xMax);
		transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
	}

	void FireLaser(){
		Vector3 startPosition = transform.position + new Vector3 (0,0.5f,0);
		GameObject beam = Instantiate(laser, startPosition, Quaternion.identity) as GameObject;
		beam.rigidbody2D.velocity = new Vector3(0,laserSpeed,0);
		AudioSource.PlayClipAtPoint (laserSound, transform.position);
	}

	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log ("Player laser collider");
		EnemyProjectile laser = collider.gameObject.GetComponent<EnemyProjectile>();
		if (laser) {
			health -= laser.GetDamage();
			laser.Hit();
			if (health <= 0){
				Die();
			}
		}
	}

	void Die(){
		LevelManager man = GameObject.Find ("LevelManager").GetComponent<LevelManager>();
		man.LoadLevel ("Win Screen");
		AudioSource.PlayClipAtPoint (destroySound, transform.position);
		Destroy(gameObject);
	}
}
